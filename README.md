# Overview
This document describes how to install [Canonical's Kubernetes Distribution](https://www.ubuntu.com/containers/kubernetes)
locally, suitable for development.

# Prerequisites
* Ubuntu 16.04 - use `lsb_release -a` to verify your version of Ubuntu
* A working internet connection - all of the files will be downloaded from various repositories

# Building
There is no software to build.  Everything we'll be installing is already packaged and ready to go.

# Installation
1. set your terminal to 132x43 to avoid complaints from the installer
1. `sudo apt update` -- make sure the package list is current
1. `sudo apt dist-upgrade` -- make sure to have the most current kernel
1. `sudo shutdown -r now` -- reboot with the new kernel
1. log back into the system
1. `sudo apt install lxd snapd` -- ensure we have the required packages installed
1. `sudo snap install conjure-up --classic` -- install `conjure-up`
1. `conjure-up --version` -- verify `conjure-up` installed. We tested with `conjure-up 2.1.5`.
You *may* have to logout and back in again to get the path correct.
1. edit `/etc/sysctl.conf` and add the values shown below
1. `sudo sysctl -p` to verify that the new settings have been applied
1. `conjure-up canonical-kubernetes` to start the installer
1. select `Deploy New Self-Hosted Controller`
1. select `localhost`
1. tab down and select `Deploy all 6 Remaining Applications`
1. wait.  This can take quite a bit of time depending on network speeds and your CPU. 20-30 minutes is not uncommon.
1. select `Run` when asked to download `kubectl` and wait for it to download
1. select `Run` when asked to check the status of the cluster
1. select `View Summary`
1. press `q` to exit the installer
1. `bin/kubectl.conjure-canonical-kubern-68d get componentstatuses` to check on components -- the `kubectl` wrapper
will be named slightly different on your installation.

## /etc/sysctl.conf
```
# K8S stuff
fs.inotify.max_user_instances = 1048576  
fs.inotify.max_queued_events = 1048576  
fs.inotify.max_user_watches = 1048576  
vm.max_map_count = 262144
```

# Tips and Tricks

## Watch Full Installation
You can [watch a full installation](http://s3.amazonaws.com/org-kurron-asciicinema/canonical-kubernetes-development.html),
which might help guide you through the menu choices and commands.

## Use `kubectl` Instead Of The Wrapper
The installer places a `kubectl` wrapper in `~/bin` but you might want to use `kubectl` directly.

1. `juju scp kubernetes-master/0:config ~/.kube/config`
1. `kubectl cluster-info`

## Interact Remotely
Kubernetes is controlled via the `kubectl` command, which can run on any machine.  We just need to install
and configure it to point to our new installation.  Perform these steps on the machine you want to interact
with the cluster on.

1. determine the [current release of Kubernetes](https://github.com/kubernetes/kubernetes/releases) and find the client download link
1. `curl --location --output /tmp/k8s.tar.gz https://dl.k8s.io/v1.6.2/kubernetes-client-linux-amd64.tar.gz` -- your version will be different
1. `tar ztvf /tmp/k8s.tar.gz` -- verify the download is good
1. `tar zxvf /tmp/k8s.tar.gz`
1. `sudo cp kubernetes/client/bin/* /usr/local/bin`
1. `sudo chmod a+x /usr/local/bin/kube*`
1. `kubectl version`
1. `cd ~`
1. `mkdir .kube`
1. `scp vagrant@192.168.253.53:.kube/config.conjure-canonical-kubern-129 ~/.kube/config` -- your ip and file name will be different
1. on your K8S box, run `kubectl proxy --address=192.168.253.53 --port 1234 --disable-filter=true` -- creates a proxy to your environment
1. edit your `~/.kube/config` so that the `server: https://10.1.2.3` matches the ip address and port from above,
for example `server: http://192.168.253.53:1234`
1. on your non-K8S box, run `kubectl cluster-info` to prove that the proxy is working correctly

## Deploying A Toy Service (hard way)
1. `kubectl run hello-world --image tutum/hello-world --replicas=1 --port=80`
1. `kubectl get deployments`
1. `kubectl get replicationcontrollers` -- ???
1. `kubectl get pods`
1. `kubectl get services`
1. `kubectl expose rc hello-world --port=8080 --type=LoadBalancer`

## Deploying A Toy Service (easy way)
1. `mkdir descriptors`
1. in a text editor, create `descriptors/hello-world.yml` with the contents shown below
1. `kubectl create --filename descriptors/hello-world.yml`
1. `kubectl get deployments`
1. `kubectl get replicationcontrollers`
1. `kubectl get pods --show-labels`
1. `kubectl describe deployments`
1. `kubectl rollout status deployment/hello-world`
1. `kubectl rollout history deployment/hello-world`
1. `kubectl expose deployment/hello-world`
1. `kubectl get svc hello-world`
1. `kubectl describe svc hello-world`
1. `curl ???`

```
apiVersion: apps/v1beta1
kind: Deployment
metadata:
    name: hello-world
spec:
    replicas: 1
    template:
        metadata:
            labels:
                app: hello-world
        spec:
            containers:
            - name: hello-world
              image: tutum/hello-world
              ports:
              - containerPort: 80
```

## Installation Into AWS

1. `sudo apt update` -- make sure the package list is current
1. `sudo apt dist-upgrade` -- make sure to have the most current kernel
1. `sudo shutdown -r now` -- reboot with the new kernel
1. log back into the system
1. `sudo apt install lxd snapd` -- ensure we have the required packages installed
1. `sudo snap install conjure-up --classic` -- install `conjure-up` and `juju`
1. `juju --version` -- verify `juju` installed. We tested with `2.1.2-xenial-amd64`.
You *may* have to logout and back in again to get the path correct.
1. `juju add-credential aws` -- use `aws` as the credential name
1. `cat ~/.local/share/juju/credentials.yaml` -- verify credentials
1. `juju set-default-region aws us-west-2` -- set default region
1. `cat ~/.local/share/juju/credentials.yaml` -- verify region
1. `juju update-clouds` -- update cloud information
1. `juju clouds` -- show cloud information
1. `juju bootstrap aws/us-west-2` -- bootstrap a controller to manage the cluster
1. `juju deploy canonical-kubernetes` -- create the cluster
1. `watch --color juju status --color` -- monitor the cluster creation
1. `juju status --color` -- final check
1. `mkdir -p ~/.kube` -- create `kubectl` configuration folder
1. `juju scp kubernetes-master/0:config ~/.kube/config` -- copy `kubectl` configuration
1. determine the [current release of Kubernetes](https://github.com/kubernetes/kubernetes/releases) and find the client download link
1. `curl --location --output /tmp/k8s.tar.gz https://dl.k8s.io/v1.6.2/kubernetes-client-linux-amd64.tar.gz` -- your version will be different
1. `tar ztvf /tmp/k8s.tar.gz` -- verify the download is good
1. `tar zxvf /tmp/k8s.tar.gz` -- unpack the bits
1. `sudo cp kubernetes/client/bin/* /usr/local/bin`
1. `sudo chmod a+x /usr/local/bin/kube*`
1. `kubectl version`
1. `kubectl cluster-info` -- verify you can interact with the cluster

## Copy AWS Configuration File
1. assumes that the remote machine has already dowloaded the `kubectl` configuration
1. `scp vagrant@192.168.253.3:.kube/config ~/.kube/aws-config` -- grab the config file from the remote machine
1. `kubectl --kubeconfig ~/.kube/aws-config config get-clusters` -- use the new file and prove that it knows about the AWS cluster
1. you can also decide to replace your `~/.kube/config` with the remote one and avoid having to always use the `--kubeconfig`
1. `mv config local-config`
1. `ln -s aws-config config`
1. `kubectl config get-clusters`

## Working With Namespaces
1. `kubectl get namespaces`
1. `kubectl create namespace foo`
1. `kubectl get namespaces`
1. `kubectl config set-context $(kubectl config current-context) --namespace=foo`

## Setting The Default Namespace
1. `kubectl get namespaces` to see what is available
1. `kubectl config set-context $(kubectl config current-context) --namespace=your-name-space`
1. `kubectl config view` to make sure the change "took"

# Troubleshooting

## Installation Errors
You can look in `~/.cache/conjure-up/conjure-up.log` for any error messages.  You might also want to
watch during the installation process: `tail --follow ~/.cache/conjure-up/conjure-up.log`

## **kubectl get componentstatuses** shows unhealthy ectd instances
The cluster works fine but according to [this bug report](https://github.com/juju-solutions/bundle-canonical-kubernetes/issues/261),
we are seeing a false negative.

## conjure-up: command not found
Log out and log back in again to get the path updated.

## Installation Hangs
The install can take quite a bit of time but, I have observed that it sometimes does not complete.
If you are installing into a virtual machine, trying increasing the RAM or CPU and trying again.
Unfortunately, we currently are not aware of any debugging techniques that can be used to see
what is causing the hang.

## SSH Into instances
There are no Amazon keys installed into the instances.  Instead, you have to access the boxes via `juju`.

* `juju ssh kubernetes-master/0`
* `juju ssh kubernetes-worker/0`
* `juju status` will give you a list of possible target machines

# License and Credits
This project is licensed under the [Apache License Version 2.0, January 2004](http://www.apache.org/licenses/).

* [conjure-up Canonical Kubernetes under LXD today!](https://insights.ubuntu.com/2016/11/21/conjure-up-canonical-kubernetes-under-lxd-today/)
* [the canonical distribution of kubernetes #30](https://jujucharms.com/canonical-kubernetes/)
* [Setting up Kubernetes with Juju](https://kubernetes.io/docs/getting-started-guides/ubuntu/installation/)
